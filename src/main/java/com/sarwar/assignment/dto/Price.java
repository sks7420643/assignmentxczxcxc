package com.sarwar.assignment.dto;

import lombok.Data;

@Data
public class Price {
    private int sku;
    private double price;
}